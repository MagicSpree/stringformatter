package com.company;

public class Main {

    public static void main(String[] args) {
        String sourceText = "A strange thing happened today. A cat with a backpack was running across the road," +
                "and I was looking at the rising sun without taking my eyes off.";
        StringFormatter stringFormatter = new StringFormatter(sourceText);
        println("Длина текста: " + sourceText.length());
        println("Количество знаков препинания: " + stringFormatter.getCountPunctuation());
        println("Текст является палиндромом: " + (stringFormatter.isPalindromeWithStringBuffer() ? "Да" : "Нет"));
        println("Вывод каждого слова из текста на новой строке: ");
        stringFormatter.splittingAString().forEach(Main::println);
    }

    private static void println(String message) {
        System.out.println(message);
    }
}
