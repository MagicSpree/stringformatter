package com.company;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StringFormatter {

    private final String REGEX_PUNCTUATION = "[!.,;:?\\-]";
    private final String REGEX_WORDS = "[\\s!.,;:?\\-]+";

    private String sourceText;

    public StringFormatter(String sourceText) {
        this.sourceText = sourceText;
    }

    public StringFormatter() {
    }

    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    /**
     * Подсчет количества знаков препинания
     *
     * @return количество знаков препинания
     */
    public int getCountPunctuation() {
        Pattern pattern = Pattern.compile(REGEX_PUNCTUATION);
        Matcher matcher = pattern.matcher(sourceText);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    /**
     * Проверка на палиндром с помощью Stream API
     *
     * @return boolean
     */
    public boolean isPalindromeWithStream() {
        String temp = sourceText.replaceAll(REGEX_WORDS, "").toLowerCase();
        return IntStream.range(0, temp.length() / 2)
                .noneMatch(i -> temp.charAt(i) != temp.charAt(temp.length() - i - 1));
    }

    /**
     * Проверка на палиндром с помощью String Buffer
     *
     * @return boolean
     */
    public boolean isPalindromeWithStringBuffer() {
        String cleanString = sourceText.replaceAll(REGEX_WORDS, "").toLowerCase();
        StringBuilder plainString = new StringBuilder(cleanString);
        StringBuilder reverseString = plainString.reverse();
        return (reverseString.toString()).equals(cleanString);
    }

    /**
     * Разделение предложения на отдельные слова
     *
     * @return List со словами
     */
    public List<String> splittingAString() {
        return Arrays.stream(sourceText.split(REGEX_WORDS))
                .collect(Collectors.toList());
    }
}
